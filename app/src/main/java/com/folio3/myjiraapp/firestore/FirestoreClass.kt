package com.folio3.myjiraapp.firestore

import android.app.Activity
import android.util.Log
import com.folio3.myjiraapp.activities.BaseActivity
import com.folio3.myjiraapp.activities.ProjectsActivity
import com.folio3.myjiraapp.models.Project
import com.folio3.myjiraapp.models.Task
import com.folio3.myjiraapp.utils.Constants
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions

class FirestoreClass {
    private val mFirestore = FirebaseFirestore.getInstance()


    // ====== Work related for Projects ======
    // START
    fun createProject(activity: ProjectsActivity, projectInfo: Project) {

        mFirestore.collection(Constants.PROJECTS)
            .document(projectInfo.id)
            .set(projectInfo, SetOptions.merge())
            .addOnSuccessListener {
                activity.projectCreationSuccess()
            }
            .addOnFailureListener { e ->

                activity.hideProgressDialog()
                Log.e(
                    activity.javaClass.simpleName,
                    "Error while adding Project"
                )

            }
    }

    fun getAllProjects(activity: Activity) {

        mFirestore.collection(Constants.PROJECTS)
            .get()
            .addOnSuccessListener { document ->
                Log.i(activity.javaClass.simpleName, document.toString())

                val projectsList = document.toObjects(Project::class.java)

                when (activity) {
                    is ProjectsActivity -> {
                        activity.fetchAllProjectsSuccess(projectsList)
                    }
                }

            }
    }
    // END

    // ====== Work related for Tasks ======
    // START

    fun createTaskInProject(activity: Activity, projectId: String, taskInfo: Task) {

        mFirestore.collection(Constants.PROJECTS)
            .document(projectId)
            .collection(Constants.TASKS).document(taskInfo.id)
            .set(taskInfo, SetOptions.merge())
            .addOnSuccessListener {

                when (activity) {
                    is BaseActivity -> {
                        activity.showErrorSnackBar(
                            "Task Added Successfully.",
                            false
                        )
                    }
                }
            }
            .addOnFailureListener {

                when (activity) {
                    is BaseActivity -> {
                        activity.showErrorSnackBar(
                            "Failed to fetch Project: $projectId Tasks",
                            true
                        )
                    }
                }

            }

    }

    fun getTasksOfProject(activity: Activity, projectId: String) {

        mFirestore
            .collection(Constants.PROJECTS)
            .document(projectId)
            .collection(Constants.TASKS)
            .get()
            .addOnSuccessListener { document ->
                Log.i(activity.javaClass.simpleName, document.toString())

                val tasksListOfProject = document.toObjects(Task::class.java)

                Log.i(activity.javaClass.simpleName,tasksListOfProject.toString())

            }
    }
    // END
}
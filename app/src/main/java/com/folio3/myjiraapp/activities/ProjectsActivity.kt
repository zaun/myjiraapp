package com.folio3.myjiraapp.activities

import android.app.Dialog
import android.graphics.Point
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.Toast
import com.folio3.myjiraapp.R
import com.folio3.myjiraapp.firestore.FirestoreClass
import com.folio3.myjiraapp.models.Project
import com.folio3.myjiraapp.models.Task
import kotlinx.android.synthetic.main.activity_projects.*
import kotlinx.android.synthetic.main.dialog_create_update_project.*
import kotlinx.android.synthetic.main.dialog_progress.*

class ProjectsActivity : BaseActivity() {

    private lateinit var mCreateProjectDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_projects)
        setupActionBar()

        setupProjectsRecyclerAdapter()

        fetchAllProjectsFromServer()
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar_projects_activity)

        val actionBar = supportActionBar;
        if (actionBar != null) {
            actionBar.title = "Projects"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val menuInflater: MenuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_projects_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.menu_add_project -> {

                val taskInfo = Task(
                    "1234",
                    "As a user I should add new Task",
                    "Desc > Task 2 should be added with Closed Status",
                    "Resolved"
                )

                FirestoreClass().createTaskInProject(
                    this,
                    "1204802733",
                    taskInfo
                )

                //showAddProjectDialog()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    // ====== Work to show Projects ======
    // START

    private fun setupProjectsRecyclerAdapter() {

    }

    private fun fetchAllProjectsFromServer() {

        showProgressDialog("Loading Projects...")

        //1. Fetch all the projects form Firestore
        FirestoreClass().getAllProjects(this@ProjectsActivity)
    }

    fun fetchAllProjectsSuccess(list: List<Project>) {

        hideProgressDialog()
        Log.i(this.javaClass.simpleName, list.toString())
        val firstProjectInfo: Project = list[0]

        FirestoreClass().getTasksOfProject(this, firstProjectInfo.id)

    }

    // END

    // ====== Work to show Add Project ======
    // START
    private fun showAddProjectDialog() {

        mCreateProjectDialog = Dialog(this@ProjectsActivity)

        mCreateProjectDialog.setContentView(R.layout.dialog_create_update_project)

        mCreateProjectDialog.setCancelable(false)
        mCreateProjectDialog.setCanceledOnTouchOutside(false)

        // Store access variables for window and blank point

        // Store access variables for window and blank point
        val window: Window? = mCreateProjectDialog.getWindow()

        val size = Point()
        // Store dimensions of the screen in `size`
        // Store dimensions of the screen in `size`
        val display = window?.windowManager?.defaultDisplay
        display?.getSize(size)
        // Set the width of the dialog proportional to 75% of the screen width
        // Set the width of the dialog proportional to 75% of the screen width
        window?.setLayout((size.x * 0.95).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        //window?.setBackgroundDrawable(resources.getDrawable(R.drawable.fragment_dialog_background))
        window?.setGravity(Gravity.CENTER)
        // Call super onResume after sizing

        //Handle Views
        mCreateProjectDialog.btn_save.setOnClickListener {

            //TODO create project into firestore.
            createProject()

        }

        mCreateProjectDialog.btn_cancel.setOnClickListener {
            mCreateProjectDialog.dismiss()
        }

        mCreateProjectDialog.show()

    }

    /**
     * A Function to validate the entries of create/edit project
     */
    private fun validateProjectDetails(): Boolean {
        mCreateProjectDialog.til_project_name.isErrorEnabled = false
        mCreateProjectDialog.til_project_desc.isErrorEnabled = false

        return when {

            TextUtils.isEmpty(
                mCreateProjectDialog.et_project_name.text.toString().trim { it <= ' ' }) -> {
                //showErrorSnackBar("Please enter project name.", true)
                mCreateProjectDialog.til_project_name.isErrorEnabled = true
                mCreateProjectDialog.til_project_name.error = "Please enter project name."
                false
            }

            TextUtils.isEmpty(
                mCreateProjectDialog.et_project_desc.text.toString().trim { it <= ' ' }) -> {
                //showErrorSnackBar("Please enter project description.", false)
                mCreateProjectDialog.til_project_desc.isErrorEnabled = true
                mCreateProjectDialog.til_project_desc.error = "Please enter project description."
                false
            }

            else -> {
                true
            }
        }

    }

    private fun createProject() {

        if (validateProjectDetails()) {

            showProgressDialog("Creating Project...")

            val projectName: String =
                mCreateProjectDialog.et_project_name.text.toString().trim { it <= ' ' }
            val projectDesc: String =
                mCreateProjectDialog.et_project_desc.text.toString().trim { it <= ' ' }

            val project = Project(
                projectName.hashCode().toString(),
                projectName,
                projectDesc
            )

            FirestoreClass().createProject(this@ProjectsActivity, project)

        }

    }

    fun projectCreationSuccess() {

        hideProgressDialog()

        Toast.makeText(this, "Project Created Successfully", Toast.LENGTH_SHORT).show()

        mCreateProjectDialog.dismiss()

    }
    // END
}
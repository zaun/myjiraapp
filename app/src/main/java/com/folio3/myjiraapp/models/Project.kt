package com.folio3.myjiraapp.models

class Project(
    val id: String = "",
    val name: String = "",
    val description: String = "",
)
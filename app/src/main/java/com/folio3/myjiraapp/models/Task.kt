package com.folio3.myjiraapp.models

class Task(
    val id: String = "",
    val title: String = "",
    val description: String = "",
    val status: String = "Active",//Active, Resolved, Closed
)
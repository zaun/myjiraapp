package com.folio3.myjiraapp.utils

object Constants {

    //Collection Names of Firestore
    // START
    const val PROJECTS = "projects"
    const val TASKS = "tasks"
    // END

    // START
    /**
     * This function will return the Task Statuses List items.
     */
    fun taskStatuses(): ArrayList<String> {
        val list = ArrayList<String>()
        list.add("Active")
        list.add("Resolved")
        list.add("Done")
        return list
    }
    // END

}